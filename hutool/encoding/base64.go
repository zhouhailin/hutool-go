package encoding

import "encoding/base64"

func (e EncodingUtil) EncodeBase64(src []byte) string {
	return base64.StdEncoding.EncodeToString(src)
}

func (e EncodingUtil) EncodeBase64Str(src string) string {
	return base64.StdEncoding.EncodeToString([]byte(src))
}

func (e EncodingUtil) EncodeBase64URL(str string) string {
	return base64.URLEncoding.EncodeToString([]byte(str))
}

func (e EncodingUtil) DecodeBase64(str string) (string, error) {
	bytes, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func (e EncodingUtil) DecodeBase64URL(str string) (string, error) {
	bytes, err := base64.URLEncoding.DecodeString(str)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
