package encoding

import "testing"

func TestBase64_Decode(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"Decode01", args{"SGVsbG9Xb3JsZA=="}, "HelloWorld", false},
		{"Decode02", args{"1SGVsbG9Xb3JsZA=="}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ba := EncodingUtil{}
			got, err := ba.DecodeBase64(tt.args.str)
			if (err != nil) != tt.wantErr {
				t.Errorf("Decode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Decode() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBase64_Encode(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Encode", args{"HelloWorld"}, "SGVsbG9Xb3JsZA=="},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ba := EncodingUtil{}
			if got := ba.EncodeBase64Str(tt.args.str); got != tt.want {
				t.Errorf("Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBase64_EncodeURL(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"EncodeURL", args{"HelloWorld"}, "SGVsbG9Xb3JsZA=="},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ba := EncodingUtil{}
			if got := ba.EncodeBase64URL(tt.args.str); got != tt.want {
				t.Errorf("EncodeURL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBase64_DecodeURL(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"DecodeURL01", args{"SGVsbG9Xb3JsZA=="}, "HelloWorld", true},
		{"DecodeURL02", args{"1SGVsbG9Xb3JsZA=="}, "", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ba := EncodingUtil{}
			got, err := ba.DecodeBase64URL(tt.args.str)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeURL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecodeURL() got = %v, want %v", got, tt.want)
			}
		})
	}
}
