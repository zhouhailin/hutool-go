package encoding

import "testing"

func TestHexUtil_Encode(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Encode", args{"HelloWorld"}, "48656c6c6f576f726c64"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			he := EncodingUtil{}
			if got := he.EncodeHexStr(tt.args.str); got != tt.want {
				t.Errorf("Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHexUtil_Decode(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"Decode01", args{"48656c6c6f576f726c64"}, "HelloWorld", false},
		{"Decode02", args{"48656c6c6f576f726c641"}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			he := EncodingUtil{}
			got, err := he.DecodeHex(tt.args.str)
			if (err != nil) != tt.wantErr {
				t.Errorf("Decode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Decode() got = %v, want %v", got, tt.want)
			}
		})
	}
}
