package encoding

import "encoding/hex"

func (e EncodingUtil) EncodeHex(src []byte) string {
	return hex.EncodeToString(src)
}

func (e EncodingUtil) EncodeHexStr(str string) string {
	return hex.EncodeToString([]byte(str))
}

func (e EncodingUtil) DecodeHex(str string) (string, error) {
	bytes, err := hex.DecodeString(str)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
