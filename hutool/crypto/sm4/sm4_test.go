package sm4

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"gitee.com/zhouhailin/hutool-go/hutool/crypto/sm"
	"testing"
)

func TestName(t *testing.T) {
	key, err := base64.StdEncoding.DecodeString("+8AO2ea5t3ZkwSZIe2Jxmw==")
	if err != nil {
		return
	}
	//key := []byte("0000000000000000")
	in := []byte("HelloWorld")

	plainTextWithPadding := sm.PKCS5Padding(in, BlockSize)
	cipherText, err := ECBEncrypt(key, plainTextWithPadding)
	if err != nil {
		t.Error(err.Error())
		return
	}
	fmt.Printf("%s\n", base64.StdEncoding.EncodeToString(cipherText))
	fmt.Printf("%x\n", cipherText)

	plainTextWithPadding, err = ECBDecrypt(key, cipherText)
	if err != nil {
		t.Error(err.Error())
		return
	}
	plainText := sm.PKCS5UnPadding(plainTextWithPadding)
	fmt.Println(string(plainText))
	if !bytes.Equal(in, plainText) {
		t.Error("decrypt result not equal expected")
		return
	}

}
