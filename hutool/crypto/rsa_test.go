package crypto

import (
	"crypto/rsa"
	"reflect"
	"testing"
)

func TestCryptoUtil_DecryptRSA(t *testing.T) {
	type args struct {
		priv       *rsa.PrivateKey
		ciphertext []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.DecryptRSA(tt.args.priv, tt.args.ciphertext)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptRSA() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecryptRSA() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_DecryptRSAStr(t *testing.T) {
	type args struct {
		priv       *rsa.PrivateKey
		ciphertext string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.DecryptRSAStr(tt.args.priv, tt.args.ciphertext)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptRSAStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecryptRSAStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSA(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSA(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSA() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncryptRSA() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSABase64(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSABase64(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSABase64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptRSABase64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSABase64Str(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSABase64Str(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSABase64Str() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptRSABase64Str() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSAHex(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSAHex(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSAHex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptRSAHex() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSAHexStr(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSAHexStr(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSAHexStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptRSAHexStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptRSAStr(t *testing.T) {
	type args struct {
		pub *rsa.PublicKey
		msg string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.EncryptRSAStr(tt.args.pub, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptRSAStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncryptRSAStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_GenerateRSAKey(t *testing.T) {
	tests := []struct {
		name    string
		want    []byte
		want1   []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, got1, err := _rsa.GenerateRSAKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateRSAKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateRSAKey() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GenerateRSAKey() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestCryptoUtil_GenerateRSAKeyBase64(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		want1   string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, got1, err := _rsa.GenerateRSAKeyBase64()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateRSAKeyBase64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GenerateRSAKeyBase64() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GenerateRSAKeyBase64() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestCryptoUtil_GenerateRSAKeyHex(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		want1   string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, got1, err := _rsa.GenerateRSAKeyHex()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateRSAKeyHex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GenerateRSAKeyHex() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GenerateRSAKeyHex() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestCryptoUtil_ParseRSAPrivateKeyStr(t *testing.T) {
	type args struct {
		privateKey string
	}
	tests := []struct {
		name    string
		args    args
		want    *rsa.PrivateKey
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.ParseRSAPrivateKeyStr(tt.args.privateKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseRSAPrivateKeyStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseRSAPrivateKeyStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_ParseRSAPublicKeyStr(t *testing.T) {
	type args struct {
		publicKey string
	}
	tests := []struct {
		name    string
		args    args
		want    *rsa.PublicKey
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_rsa := &CryptoUtil{}
			got, err := _rsa.ParseRSAPublicKeyStr(tt.args.publicKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseRSAPublicKeyStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseRSAPublicKeyStr() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerateRSAKeyFile(t *testing.T) {
	util := CryptoUtil{}
	println(util.GenerateRSAKeyFile("private.key", "public.key"))
}

func TestGenerateRSAKeyBase64_02(t *testing.T) {
	util := CryptoUtil{}
	privateKey, publicKey, err := util.GenerateRSAKeyBase64()
	if err != nil {
		t.Error(err)
	}
	t.Logf("private key : %v", privateKey)
	t.Logf("public key  : %v", publicKey)
	//privKey, err := util.ParseRSAPrivateKey(privateKey)
	privKey, err := util.ParseRSAPrivateKeyStr("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALK/3kFFpz61xPtKoFrvMf7P6Er4hrhFAAUEqEFum9voNun3iiJ4GyOHmcdkyuBz8ZWWWFmYfBuaKeT0VQK3m5bcNJKMspz0SRAXTbJKDqZ5M9vRKBXZx7qkDQI4at2BfFWrFtGo/8SwiWUWW9kXx4l6gnTuiEIYcTFXExCGT9hNAgMBAAECgYEAnLH7+QZBbYTU0GGEHxMupazGeupQ1zqNoNiLUwjOksGHWiUJL9xQFh5qHVRrUMXzEuoAO1KkAWJstWvHFiG0vtyy+sxbtpK5Xu16W/KW3oOTbcpgcg3PxC0BiDdJJP/N5RVHv6zDR5fxxfa15siCN96u+2jZULOfkZEoTMxK7NUCQQDdsPj6Gj8mvY1gZQ0c+TyMSAQY+t4bH+j5Lh8d03IvFqR/GKe2uvXukCo24YP265n15e5RbdAUKdPZUgrf1YnbAkEAzmmdaqiStg7LQiSqTh78F7wHSIkiNBSBMZJOSeKzm1r2bP9OBio2e5lIzyADIvwPvVUHhEsurahu6AixLiZi9wJBAJaf14qWqrG3uIb2IaK99NWbWK1gRehIFhK3s4ygTcSQrdNs/Qa6oL4mVYhUUUVDbqgUQYgyBF6uzLGrfJf4jmECQG0TUqQyQJUEbFSfm3980Cqaqh6imPh8fWj8r3JL4i2Oe/pNT6XiWLgn4FnTyNSL7qVWcYpvUFNkVnc3YKnKjzECQBaTJk57ZgoPlZG5P5eLoLZ7Givu76gBTqzAXw58hMFNVhJGX3xKofQguS8/8ZcgJXzQFQ7Lq29Zt1Xen/gKq7g=")
	if err != nil {
		t.Error(err)
	} else {
		t.Log("ParseRSAPrivateKeyStr success.")
	}

	//pubKey, err := util.ParseRSAPublicKey(publicKey)
	pubKey, err := util.ParseRSAPublicKeyStr("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyv95BRac+tcT7SqBa7zH+z+hK+Ia4RQAFBKhBbpvb6Dbp94oieBsjh5nHZMrgc/GVllhZmHwbmink9FUCt5uW3DSSjLKc9EkQF02ySg6meTPb0SgV2ce6pA0COGrdgXxVqxbRqP/EsIllFlvZF8eJeoJ07ohCGHExVxMQhk/YTQIDAQAB")
	if err != nil {
		t.Error(err)
	}
	enc, err := util.EncryptRSABase64Str(pubKey, "HelloWorld")
	if err != nil {
		t.Error(err)
	}
	t.Logf("EncryptRSABase64 : %s", enc)
	dec, err := util.DecryptRSAStr(privKey, "Nc/PHxZixWO1vPdR2LkYHsu7rB6heSsN52wFRMWmk16BFEvjCaYv/Nsyo/T/6Uqt2dnkOU1MyfQrb2JlpkYh/6WVgzdya5nGiErg8/1BuQ2dAK2z0nC5lr1ifpDJd8yO6apU6w9mET3UYxQSDa50bYo4tVRJDgoEkMRcpjQbyhA=")
	if err != nil {
		t.Error(err)
	}
	t.Logf("DecryptRSA : %s", dec)
}
