package crypto

import (
	"reflect"
	"testing"
)

func TestAES_Decrypt(t *testing.T) {
	type args struct {
		src []byte
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"Decrypt_01", args{[]byte{211, 39, 177, 36, 20, 58, 80, 165, 152, 91, 160, 10, 111, 186, 186, 118}, []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, "HelloWorld", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_aes := &CryptoUtil{}
			got, err := _aes.DecryptAes(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptAes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecryptAes() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAES_DecryptBase64(t *testing.T) {
	type args struct {
		src string
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"DecryptBase64_01", args{"0yexJBQ6UKWYW6AKb7q6dg==", []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, "HelloWorld", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_aes := &CryptoUtil{}
			got, err := _aes.DecryptAesBase64(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptAesBase64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecryptAesBase64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAES_DecryptHex(t *testing.T) {
	type args struct {
		src string
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"DecryptHex_01", args{"d327b124143a50a5985ba00a6fbaba76", []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, "HelloWorld", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_aes := &CryptoUtil{}
			got, err := _aes.DecryptAesHex(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptAesHex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecryptAesHex() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAES_Encrypt(t *testing.T) {
	type args struct {
		src []byte
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		// Add test cases.
		// key : 8d5538d29923e157e4d800dd81e5f545
		{"Encrypt01", args{[]byte("HelloWorld"), []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, []byte{211, 39, 177, 36, 20, 58, 80, 165, 152, 91, 160, 10, 111, 186, 186, 118}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &CryptoUtil{}
			got, err := a.EncryptAes(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptAes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncryptAes() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAES_EncryptBase64(t *testing.T) {
	type args struct {
		src []byte
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"EncryptBase64_01", args{[]byte("HelloWorld"), []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, "0yexJBQ6UKWYW6AKb7q6dg==", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_aes := &CryptoUtil{}
			got, err := _aes.EncryptAesBase64(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptAesBase64() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptAesBase64() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAES_EncryptHex(t *testing.T) {
	type args struct {
		src []byte
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"EncryptHex_01", args{[]byte("HelloWorld"), []byte{141, 85, 56, 210, 153, 35, 225, 87, 228, 216, 0, 221, 129, 229, 245, 69}}, "d327b124143a50a5985ba00a6fbaba76", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_aes := &CryptoUtil{}
			got, err := _aes.EncryptAesHex(tt.args.src, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptAesHex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptAesHex() got = %v, want %v", got, tt.want)
			}
		})
	}
}
