package crypto

import (
	"testing"
)

func TestCryptoUtil_SM3Str1(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// Add test cases.
		{"SM3_01", args{"HelloWorld"}, "44526eeba9235bae33f2bab8ff1f9ca8965b59d58be82af8111f336a00c1c432"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CryptoUtil{}
			if got := c.SM3Str(tt.args.str); got != tt.want {
				t.Errorf("SM3Str() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_EncryptSM4Base64Str(t *testing.T) {
	type args struct {
		in        string
		base64Key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"EncryptSM4Base64Str_01", args{"HelloWorld", "+8AO2ea5t3ZkwSZIe2Jxmw=="}, "znnaPFVIwgVYRgEjSjPmiQ==", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_sm := &CryptoUtil{}
			got, err := _sm.EncryptSM4Base64Str(tt.args.in, tt.args.base64Key)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptSM4Base64Str() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("EncryptSM4Base64Str() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCryptoUtil_DecryptSM4Str(t *testing.T) {
	type args struct {
		src       string
		base64Key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// Add test cases.
		{"DecryptSM4Str_01", args{"znnaPFVIwgVYRgEjSjPmiQ==", "+8AO2ea5t3ZkwSZIe2Jxmw=="}, "HelloWorld", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_sm := &CryptoUtil{}
			got, err := _sm.DecryptSM4Str(tt.args.src, tt.args.base64Key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecryptSM4Str() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("DecryptSM4Str() got = %v, want %v", got, tt.want)
			}
		})
	}
}
