package crypto

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
)

func (c CryptoUtil) Md5Hex(bytes []byte) string {
	return fmt.Sprintf("%x", md5.Sum(bytes))
}

func (c CryptoUtil) Md5HexStr(str string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

func (c CryptoUtil) Sha1Hex(bytes []byte) string {
	return fmt.Sprintf("%x", sha1.Sum(bytes))
}

func (c CryptoUtil) Sha1HexStr(str string) string {
	return fmt.Sprintf("%x", sha1.Sum([]byte(str)))
}

func (c CryptoUtil) Sha256Hex(bytes []byte) string {
	return fmt.Sprintf("%x", sha256.Sum256(bytes))
}

func (c CryptoUtil) Sha256HexStr(str string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(str)))
}

func (c CryptoUtil) Sha512Hex(bytes []byte) string {
	return fmt.Sprintf("%x", sha512.Sum512(bytes))
}

func (c CryptoUtil) Sha512HexStr(str string) string {
	return fmt.Sprintf("%x", sha512.Sum512([]byte(str)))
}
