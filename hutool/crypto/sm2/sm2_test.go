package sm2

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"testing"
)

func TestSM2(t *testing.T) {
	priv, pub, err := GenerateKey(rand.Reader)
	if err != nil {
		return
	}
	fmt.Printf("priv:%s\n", priv.D.Text(16))
	fmt.Printf("x:%s\n", pub.X.Text(16))
	fmt.Printf("y:%s\n", pub.Y.Text(16))

	println("================================")
	println(base64.StdEncoding.EncodeToString(priv.GetRawBytes()))
	println(base64.StdEncoding.EncodeToString(pub.GetRawBytes()))
	println("================================")

	publicKey2, err := base64.StdEncoding.DecodeString("wyksBidHYwczbZOB1IJZ5UZmNOCJOFrA0nxZRMXMcmQ9IhGSkdzpH/k9YtSUlc32FNPtevTNjTULQFvkcqvH2g==")
	if err != nil {
		t.Error(err)
		return
	}
	pubKey, err := RawBytesToPublicKey(publicKey2)
	if err != nil {
		t.Error(err)
		return
	}
	//encrypt, err := Encrypt(pub, []byte("HelloWorld"), C1C3C2)
	encrypt, err := Encrypt(pubKey, []byte("HelloWorld"), C1C3C2)
	if err != nil {
		t.Error(err)
		return
	}
	println(base64.StdEncoding.EncodeToString(encrypt))
	println("---------------------------------------")
	privateKey2, err := base64.StdEncoding.DecodeString("0f3c/9nDp7mnlmLl/5VRpyI/dfvMQtNl+hkzc9YwAm0=")
	if err != nil {
		t.Error(err)
		return
	}
	privKey, err := RawBytesToPrivateKey(privateKey2)
	if err != nil {
		t.Error(err)
		return
	}

	//decrypt, err := Decrypt(priv, encrypt, C1C3C2)
	decrypt2, _ := base64.StdEncoding.DecodeString("BLoPXrR2IUXWdaR9ZKHi+OwZT86PxRXwW7Cyw3dsXj77dRaCNMeARSzPLK0KUngsv2MybI1/ap6JoWXyaTEZChibpYr+Gx1AsDQwQtcy3Wiz9rfBaU71l8dzsdoxFA9R/8oHDX7TR9IAc2M=")
	decrypt, err := Decrypt(privKey, decrypt2, C1C3C2)
	if err != nil {
		t.Error(err)
		return
	}
	println(string(decrypt))
}
