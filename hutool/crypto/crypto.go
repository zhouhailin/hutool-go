package crypto

type CryptoUtil struct {
}

func isHex(src []byte) bool {
	if len(src)%2 == 1 {
		return false
	}
	for _, c := range src {
		switch {
		case '0' <= c && c <= '9':
			continue
		case 'a' <= c && c <= 'f':
			continue
		case 'A' <= c && c <= 'F':
			continue
		default:
			return false
		}
	}
	return true
}

func isHexStr(str string) bool {
	return isHex([]byte(str))
}
