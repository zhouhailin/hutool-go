package crypto

import "testing"

func Test_isHex(t *testing.T) {
	type args struct {
		src []byte
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// Add test cases.
		{"isHex_01", args{[]byte("c5YD2if3oH136UQrrvFxbO5W7ssa")}, false},
		{"isHex_02", args{[]byte("7e")}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isHex(tt.args.src); got != tt.want {
				t.Errorf("isHex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isHexStr(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// Add test cases.
		{"isHexStr_01", args{"c5YD2if3oH136UQrrvFxbO5W7ssa"}, false},
		{"isHexStr_02", args{"7e"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isHexStr(tt.args.str); got != tt.want {
				t.Errorf("isHexStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
