package crypto

import (
	"testing"
)

func TestDigestUtil_Md5Hex(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Md5Hex", args{"HelloWorld"}, "68e109f0f40ca72a15e05cc22786f8e6"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			di := CryptoUtil{}
			if got := di.Md5HexStr(tt.args.str); got != tt.want {
				t.Errorf("Md5Hex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDigestUtil_Sha1Hex(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Sha1Hex", args{"HelloWorld"}, "db8ac1c259eb89d4a131b253bacfca5f319d54f2"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			di := CryptoUtil{}
			if got := di.Sha1HexStr(tt.args.str); got != tt.want {
				t.Errorf("Sha1Hex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDigestUtil_Sha256Hex(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Sha256Hex", args{"HelloWorld"}, "872e4e50ce9990d8b041330c47c9ddd11bec6b503ae9386a99da8584e9bb12c4"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			di := CryptoUtil{}
			if got := di.Sha256HexStr(tt.args.str); got != tt.want {
				t.Errorf("Sha256Hex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDigestUtil_Sha512Hex(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"Sha512Hex", args{"HelloWorld"}, "8ae6ae71a75d3fb2e0225deeb004faf95d816a0a58093eb4cb5a3aa0f197050d7a4dc0a2d5c6fbae5fb5b0d536a0a9e6b686369fa57a027687c3630321547596"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			di := CryptoUtil{}
			if got := di.Sha512HexStr(tt.args.str); got != tt.want {
				t.Errorf("Sha512Hex() = %v, want %v", got, tt.want)
			}
		})
	}
}
