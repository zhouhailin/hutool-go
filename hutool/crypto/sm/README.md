# SM 国密算法

    https://github.com/ZZMarquis/gm/tree/master/sm2
    
    https://zhuanlan.zhihu.com/p/132352160

## SM1是一种分组加密算法

## SM2(椭圆曲线公钥密码算法)  国密椭圆曲线算法库

    它是基于椭圆曲线密码的公钥密码算法标准，其秘钥长度256bit，包含数字签名、密钥交换和公钥加密，用于替换RSA/DH/ECDSA/ECDH等国际算法。可以满足电子认证服务系统等应用需求，由国家密码管理局于2010年12月17号发布。
   
    SM2采用的是ECC 256位的一种，其安全强度比RSA 2048位高，且运算速度快于RSA。

    . 支持Generate Key, Sign, Verify基础操作
    . 支持加密和不加密的pem文件格式(加密方法参见RFC5958, 具体实现参加代码)
    . 支持证书的生成，证书的读写(接口兼容rsa和ecdsa的证书)
    . 支持证书链的操作(接口兼容rsa和ecdsa)
    . 支持crypto.Signer接口


    https://blog.csdn.net/weixin_46134229/article/details/109080567
    https://blog.csdn.net/weixin_46134229/article/details/109097592
    https://blog.csdn.net/weixin_46134229/article/details/109099485
    https://blog.csdn.net/weixin_46134229/article/details/109100287
    https://blog.csdn.net/weixin_46134229/article/details/109389991

## SM3(杂凑算法)    国密Hash算法库(摘要算法)

    用于替代MD5/SHA-1/SHA-2等国际算法，适用于数字签名和验证、消息认证码的生成与验证以及随机数的生成，可以满足电子认证服务系统等应用需求，于2010年12月17日发布。
    
    它是在SHA-256基础上改进实现的一种算法，采用Merkle-Damgard结构，消息分组长度为512bit，输出的摘要值长度为256bit。


    . 支持基础的sm3Sum操作
    . 支持hash.Hash接口


    https://blog.csdn.net/weixin_46134229/article/details/109072084
    https://blog.csdn.net/weixin_46134229/article/details/109075181

## SM4(分组算法)    国密分组密码算法库

    跟SM1类似，是我国自主设计的分组对称密码算法，用于替代DES/AES等国际算法。SM4算法与AES算法具有相同的密钥长度、分组长度，都是128bit。于2012年3月21日发布，适用于密码应用中使用分组密码的需求。

    . 支持Generate Key, Encrypt, Decrypt基础操作
    . 提供Cipher.Block接口
    . 支持加密和不加密的pem文件格式(加密方法为pem block加密, 具体函数为x509.EncryptPEMBlock)


    https://blog.csdn.net/weixin_46134229/article/details/109077138
    https://blog.csdn.net/weixin_46134229/article/details/109079807

## Fabric的国密扩展方法

    一种是基于Fabric本身扩展国密包。这种改法不用对Golang标准库做任何地改动，所有的修改都在Fabric项目源码上进行；
    其一，把国密的库进行移植，封装gm-crypto；
    其二，扩展Fabric现有的bccsp模块；
    其三，修改x509证书相关的地方。
    Fabric-CA主要是为了实现对加入联盟链的成员的身份控制以及数据生成保管。Fabric-CA中，Lib，主要是接口的实现，主要在解析申请证书请求以及签发证书流程要替换为国密算法；Util，该包数据工具类，主要在证书的编解码等操作中扩展国密算法；Vendor中，替换对Fabric的包的引用，提供对国密算法的支持
    在Fabric中扩展国密算法，大概有以下几个方面：第一，Fabric框架扩展支持国密算法；第二，Fabric-CA扩展支持国密算法；第三，Fabric-SDK扩展支持国密算法；第四，fabric-baseimage、fabric-baseos镜像扩展支持国密算法；第五，Fabirc框架扩展支持加载.so库。”
    另一种方法是基于Golang标准库扩展国密。这种方案是直接扩充Golang标准库，改法更为清晰。

