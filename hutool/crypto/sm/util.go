package sm

import (
	"bytes"
	"math/big"
)

func add(x, y *big.Int) *big.Int {
	var z big.Int
	z.Add(x, y)
	return &z
}

func sub(x, y *big.Int) *big.Int {
	var z big.Int
	z.Sub(x, y)
	return &z
}

func mod(x, y *big.Int) *big.Int {
	var z big.Int
	z.Mod(x, y)
	return &z
}

func modInverse(x, y *big.Int) *big.Int {
	var z big.Int
	z.ModInverse(x, y)
	return &z
}

func mul(x, y *big.Int) *big.Int {
	var z big.Int
	z.Mul(x, y)
	return &z
}

func lsh(x *big.Int, n uint) *big.Int {
	var z big.Int
	z.Lsh(x, n)
	return &z
}

func setBit(x *big.Int, i int, b uint) *big.Int {
	var z big.Int
	z.SetBit(x, i, b)
	return &z
}

func and(x, y *big.Int) *big.Int {
	var z big.Int
	z.And(x, y)
	return &z
}

func isEcPointInfinity(x, y *big.Int) bool {
	if x.Sign() == 0 && y.Sign() == 0 {
		return true
	}
	return false
}

func zForAffine(x, y *big.Int) *big.Int {
	z := new(big.Int)
	if x.Sign() != 0 || y.Sign() != 0 {
		z.SetInt64(1)
	}
	return z
}

func PKCS5Padding(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, padtext...)
}

func PKCS5UnPadding(src []byte) []byte {
	length := len(src)
	unpadding := int(src[length-1])
	return src[:(length - unpadding)]
}
