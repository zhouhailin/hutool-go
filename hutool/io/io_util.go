package io

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"os"
)

func (f IoUtil) ReadText(pathname string) (string, error) {
	data, err := ioutil.ReadFile(pathname)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (f IoUtil) WriteText(pathname string, text string) error {
	err := os.WriteFile(pathname, []byte(text), 0770)
	if err != nil {
		return err
	}
	return nil
}

func (f IoUtil) AppendText(pathname string, text string) error {
	file, err := os.OpenFile(pathname, os.O_CREATE|os.O_APPEND, 0770)
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()
	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(text)
	if err != nil {
		return err
	}
	err0 := writer.Flush()
	if err0 != nil {
		return err0
	}
	return nil
}

func (f IoUtil) ReadLines(pathname string) ([]string, error) {
	var lines []string
	file, err := os.OpenFile(pathname, os.O_RDONLY, 0770)
	if err != nil {
		return lines, err
	}
	defer func() { _ = file.Close() }()
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err != nil || io.EOF == err {
			break
		}
		lines = append(lines, line)
	}
	return lines, nil
}

func (f IoUtil) WriteLines(pathname string, lines []string) error {
	text := ""
	for _, line := range lines {
		text += line + "\n"
	}
	return f.WriteText(pathname, text)
}

func (f IoUtil) AppendLines(pathname string, lines []string) error {
	text := ""
	for _, line := range lines {
		text += line + "\n"
	}
	return f.AppendText(pathname, text)
}

func (f IoUtil) DeleteFile(pathname string) error {
	fileInfo, err := os.Stat(pathname)
	if err != nil {
		return err
	}
	if fileInfo.IsDir() {
		return errors.New(pathname + " is directory.")
	}
	err = os.Remove(pathname)
	if err != nil {
		return err
	}
	return nil
}
