package io

import (
	"reflect"
	"testing"
)

func TestFileUtil_AppendLines(t *testing.T) {
	type args struct {
		pathname string
		lines    []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			if err := f.AppendLines(tt.args.pathname, tt.args.lines); (err != nil) != tt.wantErr {
				t.Errorf("AppendLines() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileUtil_AppendText(t *testing.T) {
	type args struct {
		pathname string
		text     string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			if err := f.AppendText(tt.args.pathname, tt.args.text); (err != nil) != tt.wantErr {
				t.Errorf("AppendText() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileUtil_DeleteFile(t *testing.T) {
	type args struct {
		pathname string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			if err := f.DeleteFile(tt.args.pathname); (err != nil) != tt.wantErr {
				t.Errorf("DeleteFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileUtil_ReadLines(t *testing.T) {
	type args struct {
		pathname string
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			got, err := f.ReadLines(tt.args.pathname)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadLines() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadLines() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileUtil_ReadText(t *testing.T) {
	type args struct {
		pathname string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			got, err := f.ReadText(tt.args.pathname)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadText() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ReadText() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileUtil_WriteLines(t *testing.T) {
	type args struct {
		pathname string
		lines    []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			if err := f.WriteLines(tt.args.pathname, tt.args.lines); (err != nil) != tt.wantErr {
				t.Errorf("WriteLines() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileUtil_WriteText(t *testing.T) {
	type args struct {
		pathname string
		text     string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &IoUtil{}
			if err := f.WriteText(tt.args.pathname, tt.args.text); (err != nil) != tt.wantErr {
				t.Errorf("WriteText() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
