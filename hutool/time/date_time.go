package time

import (
	"strconv"
	"time"
)

// NowSeconds 当前时间的时间戳（秒）
func (dt TimeUtil) NowSeconds() int {
	return time.Now().Second()
}

// NowMillis 当前时间的时间戳（毫秒）
func (dt TimeUtil) NowMillis() int64 {
	return time.Now().UnixNano() / 1e6
}

// EpochMilli 当前时间的时间戳（毫秒）
func (dt TimeUtil) EpochMilli() int64 {
	return time.Now().UnixNano() / 1e6
}

// NowMillisStr 当前时间的时间戳（毫秒） - 字符串
func (dt TimeUtil) NowMillisStr() string {
	return strconv.FormatInt(dt.NowMillis(), 10)
}

// NowTime 当前时间的时间 : time.Time
func (dt TimeUtil) NowTime() time.Time {
	return time.Now()
}

// Now 当前时间的时间 : yyyy-MM-dd HH:mm:ss
func (dt TimeUtil) Now() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

// 2006-01-02 15:04:05.000
// FormatDateTime 格式化给定时间 : yyyy-MM-dd HH:mm:ss
func (dt TimeUtil) FormatDateTime(t time.Time) string {
	return t.Format("2006-01-02 15:04:05")
}
