package time

import "testing"

func TestDateTime_Now(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dt := &TimeUtil{}
			if got := dt.Now(); got != tt.want {
				t.Errorf("Now() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDateTime_NowMillis(t *testing.T) {
	tests := []struct {
		name string
		want int64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dt := &TimeUtil{}
			if got := dt.NowMillis(); got != tt.want {
				t.Errorf("NowMillis() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDateTime_NowMillisStr(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dt := &TimeUtil{}
			if got := dt.NowMillisStr(); got != tt.want {
				t.Errorf("NowMillisStr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestName(t *testing.T) {
	time := TimeUtil{}
	println(time.Now())
	// 1653159086969
	// 1653159064967
	println(time.NowMillisStr())
	println(time.NowMillis())
	println(time.FormatDateTime(time.NowTime()))
}
