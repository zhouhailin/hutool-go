package path

import (
	"os"
	"path/filepath"
)

func (u PathUtil) GetBinsDir() string {
	abs, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return os.TempDir()
	}
	return abs
}

func (u PathUtil) GetHomeDir() string {
	return filepath.Dir(u.GetBinsDir() + "/../")
}
