package hutool

import (
	"gitee.com/zhouhailin/hutool-go/hutool/crypto"
	"gitee.com/zhouhailin/hutool-go/hutool/encoding"
	"gitee.com/zhouhailin/hutool-go/hutool/io"
	"gitee.com/zhouhailin/hutool-go/hutool/path"
	"gitee.com/zhouhailin/hutool-go/hutool/time"
)

func EncodingUtil() encoding.EncodingUtil {
	return encoding.EncodingUtil{}
}

func CryptoUtil() crypto.CryptoUtil {
	return crypto.CryptoUtil{}
}

func IoUtil() io.IoUtil {
	return io.IoUtil{}
}

func PathUtil() path.PathUtil {
	return path.PathUtil{}
}

func TimeUtil() time.TimeUtil {
	return time.TimeUtil{}
}
