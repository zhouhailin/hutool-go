# Hutool - GO

## 目标

    轻量级工具库，不依赖第三方

## 使用

```go
import (
"gitee.com/zhouhailin/hutool-go/hutool"
"testing"
)

func TestHutool(t *testing.T) {
util := hutool.TimeUtil()
println(util.Now())
encodingUtil := hutool.EncodingUtil()
println(encodingUtil.EncodeHexStr("HelloWorld"))
}
```

```text
=== RUN   TestHutool
2022-05-22 14:32:12
48656c6c6f576f726c64
--- PASS: TestHutool (0.03s)
PASS
```

